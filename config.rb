# Reload the browser automatically whenever files change
configure :development do
  activate :livereload, no_swf: true, port: 35_720
end

# Necessary to manage plugin assets
activate :sprockets do |c|
  c.expose_middleman_helpers = true
end

# Build-specific configuration
configure :build do
  # For example, change the Compass output style for deployment
  activate :minify_css

  # Minify Javascript on build
  activate :minify_javascript

  # Use relative URLs
  activate :relative_assets
end

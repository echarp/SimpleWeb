version = 'v20241228'

$ ->
	#	Get this file's url
	src = $('head script').filter ->
		$(this).attr('src')?.match /all.js/
	.first().attr 'src'

	$('head').append """
		<title>🕸 Simple Web</title>
		<meta name='viewport' content='width=device-width, initial-scale=1' />
		<link href="#{src.replace '/javascripts/all.js', '/images/acoeuroGnu.png'}"
			rel='icon' type='image/png' />
		<script defer
			src="https://cdnjs.cloudflare.com/ajax/libs/tinymce/7.6.0/tinymce.min.js"
			integrity="sha512-/4EpSbZW47rO/cUIb0AMRs/xWwE8pyOLf8eiDWQ6sQash5RP1Cl8Zi2aqa4QEufjeqnzTK8CLZWX7J5ZjLcc1Q=="
			crossorigin="anonymous" referrerpolicy="no-referrer">
		</script>
		<script defer
			src="https://cdnjs.cloudflare.com/ajax/libs/ace/1.37.1/ace.js"
			integrity="sha512-qLBIClcHlfxpnqKe1lEJQGuilUZMD+Emm/OVMPgAmU2+o3+R5W7Tq2Ov28AZqWqZL8Jjf0pJHQZbxK9B9xMusA=="
			crossorigin="anonymous" referrerpolicy="no-referrer">
		</script>
	"""

	$('header').append "<a href='https://framagit.org/simpleweb' target='_blank'>#{version}</a>"

	$('nav').after """
		<aside>
			<form id='new_dir'>
				<label>
					<span class='symbol'>📂</span>
					<span class='plus'>+</span>
					<input class='dirname' name='dirname' />
				</label>
				<button class='symbol'>✚</button>
			</form>
			<form id='upload'>
				<label>
					<span class='symbol'>📚</span>
					<input type='file' multiple='true' />
				</label>
				<button class='symbol'>✚</button>
			</form>
			<form id='new_file'>
				<label>
					<span class='symbol'>📘</span>
					<span class='plus'>+</span>
					<input class='filename' name='filename' />
				</label>
				<label>
					<span class="symbol">.</span>
					<input class='extension' name='extension'
						value='#{window.defaultExtension || ""}' />
				</label>
				<button class='symbol'>✚</button>
			</form>
		</aside>
	"""

	# Add right hand side forms, the CMS tools
	$('body').addClass('simpleWeb').addClass('index').append """
		<main>
			<iframe id='viewer' name='viewer' frameborder='0' width='100%'></iframe>

			<form id='edit'>
				<label for='filecontent'><button class='symbol'>💾</button></label>
				<textarea id='filecontent' name='filecontent'></textarea>
				<div id='editor' />
			</form>
		</main>
	"""

	window.setupPanel()

window.setupPanel = ->
	$('tr').each ->
		tr = $(this)
		type = tr.find('img[alt]').attr 'alt'
		tr.attr 'type', type
		href = tr.find('td:last-child a').attr 'href'

		if not href
			tr.addClass 'domain selected'
				.find 'img'
				.replaceWith "<a href='/'><h1>✍</h1></a>"
				.end()
				.find 'th:last-child'
				.html "<a href='/'><h1 class='domain'>#{location.hostname.replace('edit.', '').split('.')[0]}</h1></a>"

		else if type == '[PARENTDIR]'
			$('tr.selected').removeClass 'selected'
			tr.addClass 'parent'
				.find 'img'
				.replaceWith '<h2>📂</h2>'
				.end()
				.find 'td:last-child a'
				.html "<h2 class='parent'>#{href}</h2>"
				.end()
				# Add a current directory line, to show where user is
				.after """
					<tr class='current selected'>
						<td><a href="#{location}"><h3>📖</h3></a></td>
						<td>
							<a href="#{location}">
								<h3>#{location.pathname.split('/').reverse()[1] || '✵'}/</h3>
							</a>
						</td>
					</tr>
				"""
				$('tr.parent').remove() if href == '/'

		else if type == '[DIR]'
			tr.find('img').replaceWith '📁'

		else if type == '[TXT]'
			tr.find('img').replaceWith '📘'

		else if type == '[IMG]'
			tr.find 'a'
				.attr 'target', 'viewer'
				.find 'img'
				.addClass 'preview'
				.attr 'src', href

		unless tr.hasClass 'parent' or tr.hasClass 'current'
			tr.find('td:last-child').prepend """
				<a class='delete symbol' href='#{href}'>🗑</a>
				<a class='move symbol' href='#{href}'>✍</a>
			"""

$ ->
	$.ajaxSetup cache: false

	$('form#new_dir').submit (event) ->
		event.preventDefault()
		dirname = $(this).find('input.dirname').val()
		$.ajax
			method: 'MKCOL'
			url: dirname
		.done (data, textStatus, jqXHR) ->
			window.location += dirname
		.fail (jqXHR, textStatus, errorThrown) ->
			displayError errorThrown

	$('form#upload input[type=file]').change ->
		selected = $('nav tr.selected td:last-child a:last-child').attr 'href'
		# Send files to webdav server using jquery ajax
		filesCount = this.files.length
		$(this.files).each (i, file) ->
			$.ajax
				method: 'PUT'
				url: file.name
				data: file
				contentType: 'multipart/form-data'
				processData: false
			.done (data, textStatus, jqXHR) ->
				if (i == filesCount - 1)
					reloadPanel selected
			.fail (jqXHR, textStatus, errorThrown) ->
				displayError errorThrown

	$('form#new_file').submit (event) ->
		event.preventDefault()
		filename = $(this).find('input.filename').val() + '.' + $(this).find('input.extension').val()
		$.ajax
			method: 'PUT'
			url: filename
		.done (data, textStatus, jqXHR) ->
			reloadPanel filename
			editUrl filename
		.fail (jqXHR, textStatus, errorThrown) ->
			displayError errorThrown

	$('form#edit').submit (event) ->
		event.preventDefault()

		data = $(this).find('textarea')
			.val()
			.replace(/(?:&amp;lt;|&lt;)%/g, "<%")
			.replace(/%(?:&amp;gt;|&gt;)/g, "%>")
		if $(this).find('textarea').data('content-type').match /markdown/
			data = saveMarkdown data

		$.ajax
			method: 'PUT'
			url: $(this).attr 'action'
			data: data
		.done (data, textStatus, jqXHR) ->
			noty
				type: 'success'
				text: 'OK'
				layout: 'topRight'
				closeWith: ['button']
				timeout: 2000
			# Initialize a counter, to stop after a while
			window.nbGetAppLog = 50
			displayAppLog()

		.fail (jqXHR, textStatus, errorThrown) ->
			displayError errorThrown

	setupNav()

# Sets up the navigation links on the left panel
setupNav = ->
	# Set selector
	$('nav a').click (event) ->
		elt = $(this)

		if elt.hasClass 'symbol'
			url = elt.attr 'href'
			selected = $('nav tr.selected td:last-child a:last-child').attr 'href'
		else
			$('tr.selected').removeClass 'selected'
			elt.closest('tr').addClass 'selected'

		if elt.hasClass('symbol') or elt.parents('[type="[TXT]"]').length
			event.preventDefault()

		if elt.is '[target]'
			showUrl elt.attr 'href'

		else if elt.hasClass 'move'
			moveUrl elt.attr 'href'

		else if elt.hasClass 'delete'
			deleteUrl elt.attr 'href'

		else if elt.parents('[type="[TXT]"]').length
			editUrl elt.attr 'href'

	# So that any eventual existing errors can still be displayed
	displayAppLog()

showUrl = (address) ->
	$('body').removeClass 'index'
		.removeClass 'edit'
		.addClass 'show'

moveUrl = (url) ->
	try
		destination = prompt window.location, decodeURI url
	catch URIError
		destination = prompt window.location, url
	if destination
		selected = $('nav tr.selected td:last-child a:last-child').attr 'href'
		$.ajax
			method: 'MOVE'
			url: url
			beforeSend: (xhr) ->
				xhr.setRequestHeader 'Destination', window.location + destination
		.done (data, textStatus, jqXHR) ->
			if selected == url
				window.location.reload()
			else
				reloadPanel selected
		.fail (jqXHR, textStatus, errorThrown) ->
			displayError errorThrown

deleteUrl = (url) ->
	return unless confirm ' => 🗑'
	selected = $('nav tr.selected td:last-child a:last-child').attr 'href'
	$.ajax
		method: 'DELETE'
		url: url
	.done (data, textStatus, jqXHR) ->
		if selected == url
			window.location.reload()
		else
			$("nav a[href='#{url}']").closest 'tr'
				.remove()
	.fail (jqXHR, textStatus, errorThrown) ->
		displayError errorThrown

editUrl = (address) ->
	$('body').removeClass 'index'
		.removeClass 'show'
		.addClass 'edit'

	# Setup file name
	$('form#edit').attr 'action', address

	# Load the file's content
	$.ajax
		url: address
		dataType: 'text'
	.done (response, status, xhr) ->
		# Hide the mechanisms available for edition
		tinymce.remove()

		content_type = xhr.getResponseHeader 'content-type'
		# console.log "Content type #{content_type}, matching #{content_type.match /text\/(html|markdown)/}"

		if content_type.match /text\/(html|markdown)/
			$('form#edit textarea#filecontent').val(loadMarkdown(response || window.archetype))
				.data 'content-type', content_type
			window.setupTinymce content_type
		else
			$('form#edit textarea#filecontent').val(response)
				.data 'content-type', content_type
			window.setupAce()

displayError = (errorThrown) ->
	noty
		type: 'error'
		text: errorThrown
		layout: 'topRight'
		closeWith: ['button']
		timeout: 20000

reloadPanel = (selected) ->
	$('nav table').load window.location + " nav table tbody", ->
		window.setupPanel()
		setupNav()
		$('tr.selected').removeClass 'selected'
		$("nav td:last-child a:last-child[href='#{encodeURI(selected).toLowerCase()}']")
			.closest 'tr'
			.addClass 'selected'

# Attempt to fetch and display the application log, over the next minute
displayAppLog = ->
	# Remove eventual error already present
	$('nav tr.error').removeClass 'error'
	setTimeout ->
		$.ajax
			url: '/app.log'
			dataType: 'text'
			mimeType: 'text/plain'
		.done (data, textStatus, jqXHR) ->
			# Stopping conditions: finished or number of attempts not reached
			if not data.includes('Total in ') && window.nbGetAppLog--
				displayAppLog()

			else if /ERROR/i.test data
				# Get all errors
				text = data.match /ERROR .*/gi
					.join '<br />'
					.replace /ERROR \d\d\d\d.\d\d.\d\d \d\d.\d\d.\d\d /g, ''
					.replace /.var.simpleWeb.apps..*.content./, ''

				return unless text.match(/".*/)

				# Add some error, a red shadow, to the directory/file in error
				line = text.match(/".*/)[0]
					.replace '"', ''
					.replace /:.*/, ''
				file = line.split '/'
					.reverse()[0]
				$("nav a[href='#{encodeURI file}']")
					.closest 'tr'
					.addClass 'error'
				dir = line.replace file, ''
				$("nav a[href|='#{encodeURI dir}']")
					.closest 'tr'
					.addClass 'error'

				if not not text
					displayError text
	, 1000

# Setup tinymce editor
window.setupTinymce = (content_type) ->
	$('#editor, form#edit button').hide()
	$('form#edit textarea#filecontent').css('display', 'flex')

	src = $('head script').filter ->
		$(this).attr('src')?.match /all.js/
	.first().attr 'src'

	options =
		schema: 'html5'
		resize: false
		menubar: false
		branding: false
		language: 'fr_FR'
		language_url: src.replace('all', 'fr_FR')
		selector: 'textarea#filecontent'
		statusbar: false
		auto_focus: 'filecontent'
		license_key: 'gpl'
		convert_urls: false
		image_caption: true
		entity_encoding : 'raw'
		paste_data_images: false
		remove_linebreaks: false
		browser_spellcheck: true
		link_context_toolbar: true
		quickbars_insert_toolbar: 'link image media charmap emoticons table insertdatetime'
		quickbars_selection_toolbar: 'removeformat bold italic strikethrough superscript subscript blockquote forecolor backcolor quicklink'
		save_onsavecallback: (e) ->
			$(e.formElement).submit()
		plugins: 'autosave, save, lists, advlist, autolink, link, image, media,
			charmap, preview, table, emoticons, fullscreen,
			searchreplace, insertdatetime, visualblocks, visualchars,
			wordcount, code, importcss, quickbars'
		content_css: window.stylesheetToUse # Let's import some external stylesheets!
		toolbar: [
			' save cut copy paste
			| undo redo
			| link image media charmap emoticons table insertdatetime
			| code visualblocks searchreplace fullscreen',
			' removeformat bold italic strikethrough superscript subscript blockquote forecolor backcolor
			| bullist numlist outdent indent
			| alignleft aligncenter alignright alignjustify'
		]
		paste_preprocess: (plugin, args) =>
			args.content = processPasteContent args.content

	tinyMCE.init options

# Users can drag and drop nearby images
processPasteContent = (content) =>
	if content.indexOf window.location.origin >= 0
		content.replace 'a href', 'img src'
			.replace window.location.origin, ''
			.replace /">.*a>/, '" alt="" />'
